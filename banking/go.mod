module banking

go 1.13

require (
	github.com/ashishjuyal/banking-lib v1.0.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.4.0
	golang.org/x/text v0.3.0
	gorm.io/gorm v1.23.8
)
