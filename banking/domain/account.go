package domain

import (
	"banking/dto"
	"banking/errs"
)

const dbTSLayout = "2006-01-02 15:04:05"

type Account struct {
	AccountID   string
	CustomerID  string
	OpeningDate string
	AccountType string
	Amount      float64
	Status      string
}

func (a Account) ToNewAccountResponseDto() *dto.NewAccountResponse {
	return &dto.NewAccountResponse{a.AccountID}
}

type AccountRepository interface {
	Save(Account) (*Account, *errs.AppError)
}

func NewAccount(customerId, accountType string, amount float64) Account {
	return Account{
		CustomerID:  customerId,
		OpeningDate: dbTSLayout,
		AccountType: accountType,
		Amount:      amount,
		Status:      "1",
	}
}
